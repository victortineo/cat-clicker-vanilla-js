$(function(){
    var model = {
        cats: [
            {
                name: "john",
                img: "cat1.jpg",
                click: 0,
                id: 0
            },
            {
                name: "ruppet",
                img: "cat2.jpg",
                click: 0,
                id: 1
            },
            {
                name: "dorian and lyly",
                img: "cat3.jpg",
                click: 0,
                id: 2
            }
        ],
        
        getAllCats: function(){
            return this.cats
        },

        getSingleCat: function(id){
            return this.cats[id]
        },

        setClicked: function(cat){
            cat.click += 1;
        },
        setClick: function(cat, newClick){
            newClick = Number(newClick);
            cat.click = newClick;
        },
        setName: function(cat, newName){
            cat.name = newName;
        },
        setImg: function(cat, newImg){
            cat.img = newImg;
        }
    };

    var controler = {
        current_cat: {},
        getCat: function(id){
            return model.getSingleCat(id);
        },
        getCats: function(){
            return model.getAllCats()
        },
        setCurrentCat: function(cat){
            this.current_cat = cat
        },
        showCurrentCat: function(cat) {
            this.current_cat = cat;
            catFocusView.renderFocusCat(cat);
        },
        catClicked: function(cat = this.current_cat){
            model.setClicked(cat);
            catFocusView.renderFocusCat(cat);
            adminFormView.populate(cat);
            // view function
        },
        callFocusCat: function(cat){
            this.setCurrentCat(cat);
            adminFormView.populate(cat);
            console.log('ue');
            catFocusView.renderFocusCat(cat);
        },
        submitForm: function(name, img, click, cat = this.current_cat){
            model.setName(cat, name);
            model.setImg(cat, img);
            model.setClick(cat, click);
            catFocusView.renderFocusCat(cat);
            catListView.init(this.getCats());
            this.initAdmin(false);
        },
        adminMode: false,
        initAdmin: function(isAdmin = this.adminMode){
            this.adminMode = isAdmin;
            if(this.adminMode){
                adminFormView.showAdmin()
            } else {
                adminFormView.hideAdmin()
            }
        },
        init: function(){
            this.showCurrentCat(this.getCat(0));
            this.initAdmin()
            catListView.init(this.getCats());
            catFocusView.init(this.current_cat);
            adminFormView.init(this.current_cat);
        }
    };

    var catListView = {
        renderCatList: function(cats){
            var list = $('.cat_list');
            $(list).html('');
            for (let index = 0; index < cats.length; index++) {
                let catItem = document.createElement('div');
                catItem.id = "cat_" + cats[index].id;
                let p = document.createElement('p');
                p.appendChild(document.createTextNode(cats[index].name));
                catItem.appendChild(p)
                $(list).append(catItem)       
            }
        },
        listenClicks: function(cats){
            for (let index = 0; index < cats.length; index++) {
                let currentId = "cat_" + cats[index].id;
                document.getElementById(currentId).addEventListener('click', function(){
                    controler.callFocusCat(cats[index]);
                })          
            }
            console.log('ue')
        },
        init: function(cats){
            this.renderCatList(cats);
            this.listenClicks(cats);
        }
    };

    var catFocusView = {
        renderFocusCat: function(cat){
            document.getElementsByClassName('cat_clicks')[0].innerHTML = cat.click
            document.getElementsByClassName('cat_name')[0].innerHTML = cat.name
            document.getElementsByClassName('cat_img')[0].src = "media/" + cat.img
            document.getElementsByClassName('cat_img')[0].id = "cat_img_id" + cat.id
        },
        listenClicks: function(){
            document.getElementsByClassName('cat_img')[0].addEventListener('click', function(){controler.catClicked()});
        },
        init: function(cat){
            this.renderFocusCat(cat);
            this.listenClicks(cat);
        }
    }
    var adminFormView = {
        populate: function(cat){
            $('#cat-name').val(cat.name)
            $('#cat-imgURL').val(cat.img)
            $('#cat-clicks').val(cat.click)
        },
        submitForm: function(){
            $('#admin_form').on('submit', function(e){
                let catName = $('#cat-name').val();
                let url = $('#cat-imgURL').val();
                let click = $('#cat-clicks').val();
                controler.submitForm(catName, url, click);
                controler.showCurrentCat(controler.current_cat);
                e.preventDefault();
            });
        },
        toggleAdmin: function(){
            $('#open_admin').on('click', function(e){             
                controler.initAdmin(!controler.adminMode);
                e.preventDefault();
            });
        },
        closeAdmin: function(){
            $('#cancelButton').on('click', function(e){             
                controler.initAdmin(false);
                e.preventDefault();
            });
        },
        showAdmin: function(){
            $('#admin_form').show()
        },
        hideAdmin: function(){
            $('#admin_form').hide()
        },
        init: function(cat){
            this.populate(cat);
            this.submitForm();
            this.toggleAdmin();
            this.closeAdmin();
        }
    }
    controler.init()
}

);

